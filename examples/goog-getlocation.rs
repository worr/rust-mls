extern crate mls;

use mls::{LocationRequest, WifiAccessPoints, GoogClient, SyncClient};

fn main() {
    let mut loc = LocationRequest::default();
    loc.wifi_access_points = Some(vec![
        WifiAccessPoints::new("b4:18:d1:e2:db:d7"),
        WifiAccessPoints::new("de:34:26:73:5f:d0"),
    ]);

    let client = GoogClient::new("test");
    let res = client.geolocate(&loc);
    println!("{:?}", res);
}

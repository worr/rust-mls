extern crate mls;

use mls::{LocationRequest, FallbackRequest, MozClient, SyncClient};

fn main() {
    let mut loc = LocationRequest::default();
    loc.fallbacks = Some(FallbackRequest {
        ipf: true,
        lacf: false,
    });

    let client = MozClient::new("test");
    let res = client.region(&loc);
    println!("{:?}", res);
}

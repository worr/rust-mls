extern crate mls;

use mls::{LocationRequestBuilder, FallbackRequestBuilder, WifiAccessPointsBuilder, MozClient, SyncClient};

fn main() {
    let loc = LocationRequestBuilder::default()
        .wifi_access_points(vec![
            WifiAccessPointsBuilder::default()
                .mac_address("01:23:45:67:89:cd")
                .build()
                .unwrap(),
            WifiAccessPointsBuilder::default()
                .mac_address("01:23:45:67:89:cd")
                .build()
                .unwrap(),
            ])
        .fallbacks(FallbackRequestBuilder::default()
                   .ipf(true)
                   .build()
                   .unwrap())
        .build()
        .unwrap();

    let client = MozClient::new("test");
    let res = client.geolocate(&loc);
    println!("{:?}", res);
}

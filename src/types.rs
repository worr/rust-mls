#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub enum Fallback {
    Lacf,
    Ipf,
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub enum RadioType {
    Gsm,
    Wcdma,
    Lte,
}

#[derive(Builder, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct BluetoothBeacon {
    #[builder(setter(into))]
    pub mac_address: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub age: Option<u64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    #[builder(setter(into))]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub signal_strength: Option<i64>,
}

impl BluetoothBeacon {
    pub fn new<S: Into<String>>(mac_address: S) -> Self {
        BluetoothBeacon {
            mac_address: mac_address.into(),
            age: None,
            name: None,
            signal_strength: None,
        }
    }
}

#[derive(Builder, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct CellTowers {
    pub radio_type: RadioType,
    pub mobile_country_code: RadioType,
    pub mobile_network_code: u16,
    pub location_area_code: u16,
    pub cell_id: u32,
    pub signal_strength: f64,
}

#[derive(Builder, PartialEq, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct WifiAccessPoints {
    #[builder(setter(into))]
    pub mac_address: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub age: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub channel: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub frequency: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub signal_strength: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub signal_to_noise_ratio: Option<u16>,
}

impl WifiAccessPoints {
    pub fn new<S: Into<String>>(mac_address: S) -> Self {
        WifiAccessPoints {
            mac_address: mac_address.into(),
            age: None,
            channel: None,
            frequency: None,
            signal_strength: None,
            signal_to_noise_ratio: None,
        }
    }
}

#[derive(Builder, PartialEq, Debug, Clone, Serialize, Deserialize)]
#[builder(default)]
#[serde(rename_all = "camelCase")]
pub struct FallbackRequest {
    pub lacf: bool,
    pub ipf: bool,
}

impl Default for FallbackRequest {
    fn default() -> Self {
        FallbackRequest {
            lacf: false,
            ipf: false,
        }
    }
}

#[derive(Builder, PartialEq, Debug, Clone, Serialize, Deserialize)]
#[builder(default)]
#[serde(rename_all = "camelCase")]
pub struct LocationRequest {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(setter(into))]
    pub carrier: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub consider_ip: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub home_mobile_country_code: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub home_mobile_network_code: Option<u16>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bluetooth_beacons: Option<Vec<BluetoothBeacon>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cell_towers: Option<Vec<CellTowers>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub wifi_access_points: Option<Vec<WifiAccessPoints>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fallbacks: Option<FallbackRequest>,
}

impl Default for LocationRequest {
    fn default() -> Self {
        LocationRequest {
            carrier: None,
            consider_ip: None,
            home_mobile_country_code: None,
            home_mobile_network_code: None,
            bluetooth_beacons: None,
            cell_towers: None,
            wifi_access_points: None,
            fallbacks: None,
        }
    }
}

#[derive(Builder, PartialEq, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Position {
    pub latitude: f64,
    pub longitude: f64,
    pub accuracy: f64,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub age: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub altitude: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub heading: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub pressure: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub speed: Option<f64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    #[builder(setter(into))]
    pub source: Option<String>,
}

impl Position {
    pub fn new(latitude: f64, longitude: f64, accuracy: f64) -> Self {
        Position {
            latitude: latitude,
            longitude: longitude,
            accuracy: accuracy,
            age: None,
            altitude: None,
            heading: None,
            pressure: None,
            speed: None,
            source: None,
        }
    }
}

#[derive(Builder, PartialEq, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LocationSubmission {
    pub timestamp: u64,
    pub position: Position,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub bluetooth_beacons: Option<Vec<BluetoothBeacon>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub cell_towers: Option<Vec<CellTowers>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub wifi_access_points: Option<Vec<WifiAccessPoints>>,
}

impl LocationSubmission {
    pub fn new(timestamp: u64, position: Position) -> Self {
        LocationSubmission {
            timestamp: timestamp,
            position: position,
            bluetooth_beacons: None,
            cell_towers: None,
            wifi_access_points: None,
        }
    }
}

#[derive(Builder, PartialEq, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SubmissionRequest {
    pub items: Vec<LocationSubmission>,
}

impl SubmissionRequest {
    pub fn new(locs: Vec<LocationSubmission>) -> Self {
        SubmissionRequest { items: locs }
    }
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct Region {
    pub country_code: String,
    pub country_name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fallback: Option<Fallback>,
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Gps {
    pub lat: f64,
    pub lng: f64,
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Location {
    pub location: Gps,
    pub accuracy: f64,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fallback: Option<Fallback>,
}

use reqwest;
use serde_json;
use std::error;
// need to import for error
use std::error::Error as _Error;
use std::fmt;
use std::result;
use url;

pub type Result<T> = result::Result<T, Error>;

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub(crate) struct InnerError {
    pub domain: String,
    pub reason: String,
    pub message: String,
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct ApiError {
    errors: Vec<InnerError>,
    pub code: u16,
    pub message: String,
}

#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub(crate) struct OuterError {
    pub error: ApiError,
}

#[derive(Debug)]
pub enum Error {
    ApiError(ApiError),
    ReqwestError(reqwest::Error),
    UrlError(url::ParseError),
    SerdeJsonError(serde_json::Error),
    NotImplemented,
}

impl error::Error for Error {
    fn description(&self) -> &str {
        match self {
            &Error::ReqwestError(ref e) => e.description(),
            &Error::ApiError(ref e) => e.message.as_str(),
            &Error::UrlError(ref e) => e.description(),
            &Error::SerdeJsonError(ref e) => e.description(),
            &Error::NotImplemented => "This function not implemented",
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match self {
            &Error::ReqwestError(ref e) => Some(e),
            &Error::UrlError(ref e) => Some(e),
            &Error::SerdeJsonError(ref e) => Some(e),
            &Error::ApiError(_) => None,
            &Error::NotImplemented => None,
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl From<reqwest::Error> for Error {
    fn from(e: reqwest::Error) -> Self {
        Error::ReqwestError(e)
    }
}

impl From<ApiError> for Error {
    fn from(e: ApiError) -> Self {
        Error::ApiError(e)
    }
}

impl From<url::ParseError> for Error {
    fn from(e: url::ParseError) -> Self {
        Error::UrlError(e)
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error::SerdeJsonError(e)
    }
}

use error::{OuterError, Error, Result};
use reqwest;
use types::*;
use url;

const MOZ_PREFIX: &'static str = "https://location.services.mozilla.com/";
const GOOG_PREFIX: &'static str = "https://www.googleapis.com/geolocation/";
const GEOLOCATE_V1: &'static str = "v1/geolocate";
const COUNTRY_V1: &'static str = "v1/country";
const GEOSUBMIT_V2: &'static str = "v2/geosubmit";

/// This is a trait for implementing a synchronous mls/gls
/// client. You can use this to perhaps implement mocks,
/// or implement an mls/gls-conforming client.
pub trait SyncClient {
    /// Fetches regional information based on location information, like country
    ///
    /// # Example:
    ///
    /// ```no_run
    /// use mls::{LocationRequest, FallbackRequest, MozClient, SyncClient};
    ///
    /// let mut loc = LocationRequest::default();
    /// loc.fallbacks = Some(FallbackRequest {
    ///     ipf: true,
    ///     lacf: false,
    ///  });
    ///
    /// let client = MozClient::new("test");
    /// let res = client.region(&loc);
    /// println!("{:?}", res);
    /// ```
    fn region(&self, loc: &LocationRequest) -> Result<Region>;

    /// Fetches precise location based on location information
    ///
    /// # Example:
    ///
    /// ```no_run
    /// use mls::{LocationRequest, WifiAccessPoints, MozClient, SyncClient};
    ///
    /// let mut loc = LocationRequest::default();
    /// loc.wifi_access_points = Some(vec![
    ///     WifiAccessPoints::new("01:23:45:67:89:cd"),
    ///     WifiAccessPoints::new("cd:89:67:45:23:01"),
    /// ]);
    ///
    /// let client = MozClient::new("test");
    /// let res = client.geolocate(&loc);
    /// println!("{:?}", res);
    /// ```
    fn geolocate(&self, loc: &LocationRequest) -> Result<Location>;

    /// Submits wifi/bluetooth/celltower information along with GPS
    /// coordinates to an MLS-compatible service
    fn geosubmit(&self, loc: &LocationSubmission) -> Result<()>;
}

/// An MLS client that talks specifically to Mozilla
///
/// Mozilla supports the full suite of MLS operations (obviously).
/// Register [here](https://location.services.mozilla.com/api) to get an API
/// key to use their service.
///
/// You can specify your own `reqwest::Client` if you want to specify
/// options, like a timeout.
///
/// # Example
///
/// ```
/// let moz_client = mls::MozClient::new("test");
/// ```
///
/// If you want to specify a timeout:
///
/// ```
/// # extern crate reqwest;
/// # extern crate mls;
/// use mls;
/// use reqwest;
/// use std::time::Duration;
///
/// let req_client = reqwest::Client::builder()
///     .timeout(Duration::from_secs(10))
///     .build()
///     .unwrap();
/// let mut moz_client = mls::MozClient::from(req_client);
/// moz_client.set_api_key("test");
/// ```
#[derive(Debug, Clone)]
pub struct MozClient {
    client: reqwest::Client,
    api_key: Option<String>,
}

impl MozClient {
    /// Creates a new `MozClient` with an API key and a bare
    /// `reqwest::Client`
    pub fn new<S: Into<String>>(api_key: S) -> Self {
        MozClient {
            client: reqwest::Client::new(),
            api_key: Some(api_key.into()),
        }
    }

    /// Sets the API key
    pub fn set_api_key<S: Into<String>>(&mut self, api_key: S) {
        self.api_key = Some(api_key.into());
    }

    fn make_url(&self, api: &'static str) -> Result<url::Url> {
        let mut u = url::Url::parse(MOZ_PREFIX)?;
        u = u.join(api)?;
        if self.api_key.is_some() {
            let key = self.api_key.clone();
            u.query_pairs_mut().append_pair("key", &key.unwrap());
        }
        Ok(u)
    }
}

impl From<reqwest::Client> for MozClient {
    /// Creates a `MozClient` from an existing `reqwest::Client`
    ///
    /// Consumes the `reqwest::Client`
    fn from(c: reqwest::Client) -> Self {
        MozClient {
            api_key: None,
            client: c,
        }
    }
}

impl SyncClient for MozClient {
    fn region(&self, loc: &LocationRequest) -> Result<Region> {
        let mut resp = self.client
            .get(self.make_url(COUNTRY_V1)?)
            .json(&loc)
            .send()?;

        if resp.status().is_success() {
            Ok(resp.json()?)
        } else {
            Err(Error::ApiError(resp.json::<OuterError>()?.error))
        }
    }

    fn geolocate(&self, loc: &LocationRequest) -> Result<Location> {
        let mut resp = self.client
            .post(self.make_url(GEOLOCATE_V1)?)
            .json(&loc)
            .send()?;

        if resp.status().is_success() {
            Ok(resp.json()?)
        } else {
            Err(Error::ApiError(resp.json::<OuterError>()?.error))
        }
    }

    fn geosubmit(&self, loc: &LocationSubmission) -> Result<()> {
        self.client
            .post(self.make_url(GEOSUBMIT_V2)?)
            .json(&loc)
            .send()?;
        Ok(())
    }
}

/// A GLS client that talks specifically to Google
///
/// Google supports some of the MLS options and endpoints. Specifically,
/// only the `geolocate` endpoint is supported. Not all of the `LocationRequest`
/// fields are supported. For a list of the differences, see
/// [here](https://mozilla.github.io/ichnaea/api/geolocate.html#deviations-from-gls-api).
///
/// You can follow the instructions
/// [here](https://developers.google.com/maps/documentation/geolocation/get-api-key)
/// to get an API
/// key to use their service.
///
/// You can specify your own `reqwest::Client` if you want to specify
/// options, like a timeout.
///
/// # Example
///
/// ```
/// let goog_client = mls::GoogClient::new("test");
/// ```
///
/// If you want to specify a timeout:
///
/// ```
/// # extern crate reqwest;
/// # extern crate mls;
/// use mls;
/// use reqwest;
/// use std::time::Duration;
///
/// let req_client = reqwest::Client::builder()
///     .timeout(Duration::from_secs(10))
///     .build()
///     .unwrap();
/// let mut goog_client = mls::GoogClient::from(req_client);
/// goog_client.set_api_key("test");
/// ```
#[derive(Debug, Clone)]
pub struct GoogClient {
    client: reqwest::Client,
    api_key: Option<String>,
}

impl GoogClient {
    /// Creates a new `GoogClient` with an API key and a bare
    /// `reqwest::Client`
    pub fn new<S: Into<String>>(api_key: S) -> Self {
        GoogClient {
            client: reqwest::Client::new(),
            api_key: Some(api_key.into()),
        }
    }

    /// Sets the API key
    pub fn set_api_key<S: Into<String>>(&mut self, api_key: S) {
        self.api_key = Some(api_key.into());
    }

    fn make_url(&self, api: &'static str) -> Result<url::Url> {
        let mut u = url::Url::parse(GOOG_PREFIX)?;
        u = u.join(api)?;
        if self.api_key.is_some() {
            let key = self.api_key.clone();
            u.query_pairs_mut().append_pair("key", &key.unwrap());
        }
        Ok(u)
    }
}

impl From<reqwest::Client> for GoogClient {
    /// Creates a `GoogClient` from an existing `reqwest::Client`
    ///
    /// Consumes the `reqwest::Client`
    fn from(c: reqwest::Client) -> Self {
        GoogClient {
            api_key: None,
            client: c,
        }
    }
}

impl SyncClient for GoogClient {
    /// # Errors
    /// Not implemented in GLS
    fn region(&self, _loc: &LocationRequest) -> Result<Region> {
        Err(Error::NotImplemented)
    }

    fn geolocate(&self, loc: &LocationRequest) -> Result<Location> {
        let mut resp = self.client
            .post(self.make_url(GEOLOCATE_V1)?)
            .json(&loc)
            .send()?;

        if resp.status().is_success() {
            Ok(resp.json()?)
        } else {
            Err(Error::ApiError(resp.json::<OuterError>()?.error))
        }
    }

    /// # Errors
    /// Not implemented in GLS
    fn geosubmit(&self, _loc: &LocationSubmission) -> Result<()> {
        Err(Error::NotImplemented)
    }
}
